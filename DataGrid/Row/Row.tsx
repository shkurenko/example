import * as React from "react";

import useStyles from "../styles";

export const DataGridRow: React.FC = ({ children }) => {
  const classes = useStyles();

  return <div className={classes.row}>{children}</div>;
};
