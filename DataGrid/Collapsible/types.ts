export interface DataGridCollapsibleProps {
  value: boolean;
  onChange: () => void;
}

export default DataGridCollapsibleProps;
