import * as React from "react";
import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles(() => ({
  root: {
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
}));

export const DataGridColumnText: React.FC = (props) => {
  const classes = useStyles();
  return <div className={classes.root} {...props} />;
};
