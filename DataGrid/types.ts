export interface DataGridProps {
  colWidths: string[];
}

export default DataGridProps;
