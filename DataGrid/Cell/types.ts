interface DataGridCellProps {
  fixedLeft?: boolean;
  fixedRight?: boolean;
  borderLeft?: boolean;
  borderRight?: boolean;
}

export interface DataGridCellTheme {
  borderLeft: boolean;
  borderRight: boolean;
}

export default DataGridCellProps;
