import * as React from "react";

import DataGridCellProps from "./types";
import useStyles from "../styles";

export const DataGridCell: React.FC<DataGridCellProps> = ({
  children,
  fixedLeft = false,
  fixedRight = false,
  borderLeft = false,
  borderRight = false,
}) => {
  const classes = useStyles();
  return (
    <div
      className={classes.cell}
      data-fixed-left={fixedLeft ? "" : null}
      data-fixed-right={fixedRight ? "" : null}
      data-border-left={borderLeft ? "" : null}
      data-border-right={borderRight ? "" : null}
    >
      {children}
    </div>
  );
};
