import * as React from "react";
import Popover, { PopoverOrigin } from "@material-ui/core/Popover";
import { Typography } from "@x5-react-uikit/core";
import { ArrowDown as ArrowDownIcon } from "@x5-react-uikit/icons";

import IconButton from "ui/IconButton";

import useStyles from "./styles";

const anchorOrigin: PopoverOrigin = {
  vertical: "bottom",
  horizontal: "right",
};

const transformOrigin: PopoverOrigin = {
  vertical: "top",
  horizontal: "right",
};

export const DataGridCollector: React.FC = ({ children }) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    },
    []
  );

  const handleClose = React.useCallback(() => {
    setAnchorEl(null);
  }, []);

  const open = Boolean(anchorEl);

  return (
    <>
      <div className={classes.button}>
        <IconButton
          variant={"ghost"}
          size={"extra-small"}
          onClick={handleClick}
        >
          <ArrowDownIcon size={"small"} />
        </IconButton>
      </div>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
        elevation={0}
        classes={{
          paper: classes.root,
        }}
      >
        {children}
      </Popover>
    </>
  );
};

export const DataGridCollectorFilter: React.FC = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.block}>
      <Typography variant={"h6"} className={classes.label}>
        Фильтрация
      </Typography>
      <div className={classes.content}>{children}</div>
    </div>
  );
};
