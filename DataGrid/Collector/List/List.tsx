import * as React from "react";
import clsx from "clsx";
import { Input, Checkbox, useScrollStyles } from "@x5-react-uikit/core";

import DataGridCollectorListProps from "./types";
import useStyles from "./styles";

export const DataGridCollectorList: React.VFC<DataGridCollectorListProps> = ({
  values,
  options,
}) => {
  const classes = useStyles();
  const classesScroll = useScrollStyles();
  const [search, setSearch] = React.useState("");
  const list = React.useMemo(() => {
    return options || [];
  }, [options, search]);
  return (
    <div className={classes.root}>
      <div className={classes.search}>
        <Input placeholder={"Поиск"} search stretch />
      </div>
      <div className={clsx(classes.list, classesScroll.root)}>
        {list.map((item) => (
          <div key={item.id} className={classes.item}>
            <Checkbox label={item.value} />
          </div>
        ))}
      </div>
    </div>
  );
};
