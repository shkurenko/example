interface DataGridCollectorListProps {
  values: number[];
  options: { id: number; value: string }[];
}

export default DataGridCollectorListProps;
