import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme) => ({
  root: {},
  search: {
    marginBottom: theme.spacing(2),
  },
  list: {
    margin: theme.spacing(0, -2),
  },
  item: {
    padding: theme.spacing(1, 2),
  },
}));

export default useStyles;
