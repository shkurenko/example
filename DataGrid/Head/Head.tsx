import * as React from "react";
import { Typography } from "@x5-react-uikit/core";
import clsx from "clsx";

import DataGridHeadProps from "./types";
import useStyles from "../styles";

export const DataGridHead: React.FC<DataGridHeadProps> = ({
  children,
  fixedLeft = false,
  fixedRight = false,
  colSpan = 1,
  rowSpan = 1,
  borderLeft = false,
  borderRight = false,
}) => {
  const classes = useStyles();
  const style = React.useMemo<Record<string, string>>(
    () => ({
      gridColumn: `span ${colSpan}`,
      gridRow: `span ${rowSpan}`,
    }),
    [colSpan, rowSpan]
  );

  return (
    <div
      className={clsx(classes.cell, classes.head)}
      data-border-left={borderLeft ? "" : null}
      data-border-right={borderRight ? "" : null}
      data-fixed-top={""}
      data-fixed-left={fixedLeft ? "" : null}
      data-fixed-right={fixedRight ? "" : null}
      style={style}
    >
      <Typography className={classes.headInner} variant={"h5"}>
        {children}
      </Typography>
    </div>
  );
};
